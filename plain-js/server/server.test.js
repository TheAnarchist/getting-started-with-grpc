const findOneById = require('./server')

test('simple test', () => {
    const testUserId = 111
    const call = {
        request: {
            user_id: testUserId
        }
    }

    function callback(error, data) {
        expect(error).toBe(null)
        expect(data.id).toBe(testUserId)
    }

    findOneById(call, callback)
})
