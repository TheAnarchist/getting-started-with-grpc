#!/bin/bash

mkdir -p ./dist

protoc \
  --proto_path=./../proto \
  --go_out=plugins=grpc,paths=source_relative:./dist \
  ./../proto/*.proto
